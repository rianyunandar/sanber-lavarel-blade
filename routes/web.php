<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/master', function()
// {
//     return view('/adminlte/master');
// });

// Route::get('/items', function ()
// {
//     return view('adminlte/items/index');
// });

// Route::get('/items/create', function ()
// {
//     return view('adminlte/items/create');
// });

// Route::get('/', function ()
// {
//     return view('adminlte/items/table');
// });

// Route::get('/data-tables', function ()
// {
//     return view('adminlte/items/data-tables');
// });


Route::get('/casts', 'CastController@index');
Route::get('/casts/create', 'CastController@create');
Route::post('/casts', 'CastController@store');
Route::get('/casts/{cast_id}', 'CastController@show');
Route::get('/casts/{cast_id}/edit', 'CastController@edit');
Route::put('/casts/{cast_id}', 'CastController@update');
Route::delete('/casts/{cast_id}', 'CastController@destroy');







